#include <stdio.h>
#include <stdlib.h>

int main() {
        printf("hello world! ");  // печатаем hello world в stdout

        FILE *file;
        file = fopen("./hello.txt","r");  // открываем файл

        int n = 0;
        fscanf(file, "%d", &n);  // считываем из файла число
        printf("%d", n);  // печатаем число в stdout

        fclose(file);
        return 0;
}