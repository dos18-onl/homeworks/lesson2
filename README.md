## Теория

1. Почитать главу "Введение" в книге "Операционные системы" А. Таненбаум (4е издание). При желании можно продолжить изучать дальше :)
Что-то может быть непонятно, приносите вопросы на обсуждение на занятие :)
1. https://highload.today/systemd-linux/ - про systemd

# !!! Любые вопросы пишите [здесь](https://docs.google.com/spreadsheets/d/10KYZrSE2WTLiPvHrmCzx7_JFxay08z7kkdMNaflFME4/edit#gid=0) !!!


## Практика

1. ### Системные вызовы

    1. Копируем hello.c в рабочую директорию, компилируем код

        `gcc hello.c -o hello.out`

    1. Создаем в той же директории файл hello.txt и записываем в него число.

    1. Запускаем программу, вывод программы должен быть таким: `hello world! <число из файла>`

    1. А теперь посмотрим вистемные вызовы! strace запишет вызываемые системные вызовы в output.txt

        `strace -o output.txt ./a.out`

    ### Вопросы:

    1. Что выполняет программа?
    1. Что будет, если файла hello.txt не будет существовать?
    1. За что отвечают системные вызовы execve, brk, read, write?

1. ### Процессы

    1. Запускаем в терминале `sleep 600`
    1. В другом терминале запускаем `ps aux`, найти запущенный процесс, найти его pid
    1. "Убиваем" процесс с помощью `kill <номер процесса>` 

    ### Вопросы:

    1. Что такое pid?
    1. Что делает sleep?
    1. Какую информацию выводит ps?

1. ### Systemd service

    1. Копируем hello.sh (выводит каждые 5 сек строчку в файл /tmp/awesomelog.log) в /usr/local/bin/

        ```
        sudo nano /usr/local/bin/hello.sh 
        копируем содержимое в файл
        sudo chmod +x /usr/local/bin/hello.sh  # Даем права на запуск файла

        ```

    1. Создаем systemd сервис для этого скрипта (копируем из awesome.service)

        ```
        sudo nano /etc/systemd/system/awesome.service
        ```

    1. Запускаем и активируем этот сервис

        ```
        sudo systemctl start awesome.service
        sudo systemctl enable awesome.service
        ```

    1. Провреяем, что в файл что-то пишется (каждые 5 сек должна добавляться новая запись)
       `tail -f /tmp/awesomelog.log`
    1. Проверяем статус сервиса
        `sudo systemctl status awesome.service`
    1. Останавливаем и деактивируем сервис
        ```
        sudo systemctl stop awesome.service
        sudo systemctl disable awesome.service
        ```
    ### Вопросы:

    1. За что отвечает параметр WantedBy? Каким он может быть?
    1. Что будет, если в команде systemctl status awesome.service не указать .service? Будет ли это работать? Почему?
